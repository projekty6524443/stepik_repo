# Witam serdecznie stepikarzu
tutaj jest pierwszy paragraf,
tutaj jest drugi paragraf
tutaj jest trzeci paragraf
**tutaj jest pogrubieie**
*tutaj jest kursywa*
>~~tutaj jest przekreslenie~~
## listy numeryczne:
1. kocham
    1. bardzo
2. mocno
    1. i szczerze
3. stepika

## listy nienumeryczne:
- lista stepikowa test
    - tutaj robie test
- lista stepikowa 2
- lista stepikowa 3
```python   
def hello_world():
    print("Hello, world!")
    print("This is a multi-line code block.")
    print("It contains at least three lines.")


'print("Hello,world")'
'print("stepik")'
'print("Bye, world")'
```

![kurak](istockphoto-1001772966-612x612.jpg)



